# Documentation

## Table des matières

- [Objectifs Pédagogiques](#objectifs-pédagogiques)
- [Description Sommaire](#description-sommaire)
- [Actions du joueur](#actions-du-joueur)
- [Informations que le jeu renvoie au joueur](#informations-que-le-jeu-renvoie-au-joueur)

## Objectifs Pédagogiques

### Objectifs pédagogiques général

*Comprendre la vie et les tentatives d’ascension sociale des Amboisiens durant la Renaissance, au vue des opportunités proposées par le statut d’Amboise de Château Royal.*

#### Description des objectifs pédagogiques
###### Les opportunités à Amboise durant la Renaissance
- Faire comprendre au joueur que pour évoluer dans la société, il doit faire des choix qui lui permettront d'obtenir des ressources et de la réputation. Lui faire comprendre que ses gains dépendent de son statut social et de ses choix.
- Il faut aussi qu'il comprenne l'enjeu de se faire de l'argent et un réseau pour que son fils puisse avoir un meilleur départ dans la vie.
- Ansi que selon son rang social, il n'aura pas accès aux mêmes ressources et que son influence sera limitée.

###### La société de caste à la Renaissance
- Faire comprendre au joueur que la société est divisée en plusieurs castes sociales et que chaque caste a ses propres avantages et inconvénients.
- Lui faire comprendre que les relations entre les différentes castes sociales sont importantes pour pouvoir évoluer dans la société.

## Description Sommaire

#### Jeu de choix et de conséquences

Nooblety est un jeu immersif qui vous transporte dans un univers médiéval se déroulant à Amboise pendant la Renaissance jusqu'au début des Guerres de religion (1450 - 1574). Vous commencerez en incarnant un jeune paysan, mais votre objectif est de gravir les échelons de la société pour devenir un noble respecté.

Votre personnage naît dans la peau d'un modeste paysan, et vous avez la liberté de choisir comment il évolue. Vous pouvez tenter de maximiser vos profits en vendant vos produits, ou bien essayer (même si cela peut s'avérer difficile) de bâtir un réseau de relations et une réputation parmi les différentes castes sociales.

Une fois votre heure arrivée, le fils de votre personnage naît, vous prendrez le contrôle de sa destinée. Vous pourrez choisir son métier en fonction des ressources et de la réputation que vous avez accumulées avec le personnage précédent. Votre nouveau personnage, un fils né avec des opportunités différentes, pourra entreprendre diverses quêtes visant à obtenir la reconnaissance de la noblesse.

Le jeu se poursuit jusqu'à ce que votre personnage atteigne le statut de noble tant convoité ou jusqu'à la fin d'un nombre prédéfini de générations. Serez-vous capable de guider votre lignée vers la noblesse et la gloire dans cet univers médiéval exigeant ?

## Actions du joueur

##### Contrôles

###### Clavier - Souris

- ZQSD / Flèches directionnelles : Permettent de se déplacer
- E : Permet d'interagir avec un personnage ou un objet

###### Manette - Contrôleur

- Joystick gauche : Permet de se déplacer
- Bouton A : Permet d'interagir avec un personnage ou un objet

##### Actions

- Se déplacer dans la ville
- Parler aux personnages
- Faire des choix

## Informations que le jeu renvoie au joueur

#### Différentes manières de s'anoblir (Opportunités)

- Se marier avec une personne de rang supérieur afin que la descendance soit de rang supérieur
- Faire des cadeaux à des personnes nobles afin de se faire anoblir
- S'acheter un titre de noblesse
- Echanger des terres contre un titre de noblesse

#### Société de caste à la Renaissance

- Division entre les différentes castes sociales
- Ce que donne accès chaque caste sociale
- Les relations entre les différentes castes sociales
